# Build script for msm8953

# Remove unused repo
rm -rf .repo/local_manifests
rm -rf prebuilts/clang/host/linux-x86/clang-proton

# Clone the local_manifests repository
git clone https://codeberg.org/aepranata/manifest.git --depth=1 --single-branch -b atiga-14.0 .repo/local_manifests

# Force sync the repository
/opt/crave/resync.sh

# Clone trees
git clone https://codeberg.org/aepranata/atiga_device_xiaomi.git --single-branch -b rosy device/xiaomi/rosy
git clone https://codeberg.org/aepranata/atiga_kernel_xiaomi.git --single-branch --recurse-submodules -b rosy kernel/xiaomi/rosy
git clone https://codeberg.org/aepranata/atiga_vendor_xiaomi.git --single-branch -b rosy vendor/xiaomi/rosy

# Set up the build environment
source build/envsetup.sh

# Lunch for msm8953 device
lunch lineage_rosy-userdebug

# Make clean build
make installclean

# Build for msm8953 device
mka bacon
